package gui;

import static org.junit.Assert.assertNotNull;

import generation.CardinalDirection;
import generation.Distance;
import gui.Constants.UserInput;
import gui.Robot.Direction;
import gui.Robot.Turn;
/**
 * This class uses the robot API to drive through a maze using
 * the distance matrix. It should take the shortest path to the
 * exit, even if that entails jumping over a wall.
 * @author Phillip Watkins
 *
 */
public class Wizard implements RobotDriver{
	private BasicRobot bot;
	private int w;
	private int h;
	private Distance distance;
	private float MAX_BATTERY = 3000;
	private boolean frontSensor, backSensor, leftSensor, rightSensor = true; // all working initially
	
	public Wizard() {
		
	}
	
	@Override
	public void setRobot(Robot r) {
		bot = (BasicRobot)r;
	}

	@Override
	public void setDimensions(int width, int height) {
		w = width;
		h = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	@Override
	public void triggerUpdateSensorInformation() {
		// adjust driver's sensor variables to match bot's
		// button will have 3 seconds delay before causing updates
		frontSensor = bot.hasOperationalSensor(Direction.FORWARD);
		backSensor = bot.hasOperationalSensor(Direction.BACKWARD);
		leftSensor = bot.hasOperationalSensor(Direction.LEFT);
		rightSensor = bot.hasOperationalSensor(Direction.RIGHT);		
	}

	@Override
	public boolean drive2Exit() throws Exception {
		// show full map and yellow path to exit by default, except when there is no panel to operate
		if ( bot.controller.panel != null) {
			bot.controller.keyDown(UserInput.ToggleLocalMap, 0);
			bot.controller.keyDown(UserInput.ToggleFullMap, 0);
			bot.controller.keyDown(UserInput.ToggleSolution, 0);
		}
		
		// This line was put here for easier testing on maze from input.xml. It is redundant when using gui normally
		distance = bot.controller.getMazeConfiguration().getMazedists();

		while( !bot.hasStopped() && !bot.isAtExit() ) {
			triggerUpdateSensorInformation();
			int x = bot.getCurrentPosition()[0];
			int y = bot.getCurrentPosition()[1];
			int currentDistanceToExit = bot.controller.getMazeConfiguration().getDistanceToExit(x, y);
			
			if ( (bot.getBatteryLevel() - bot.JUMP_COST) >= 0 ) { // if there's enough energy to jump
				
				// check in each direction to see if it is efficient to jump
				if ( bot.controller.getMazeConfiguration().isValidPosition(x+1, y)
					 && isCheaperToJump(x, y, (x+1), y)
					 && distance.getDistanceValue(x+1, y) < currentDistanceToExit ) {
						// rotate until facing the wall the Wizard chose to jump. then jump it
						rotateUntilFacingDesiredCD(CardinalDirection.East);
						bot.jump();
				} else if ( bot.controller.getMazeConfiguration().isValidPosition(x-1, y)
					 && isCheaperToJump(x, y, (x-1), y)
					 && distance.getDistanceValue(x-1, y) < currentDistanceToExit ) {
						
						rotateUntilFacingDesiredCD(CardinalDirection.West);
						bot.jump();
				} else if ( bot.controller.getMazeConfiguration().isValidPosition(x, y+1)
					 && isCheaperToJump(x, y, x, (y+1)) 
					 && distance.getDistanceValue(x, y+1) < currentDistanceToExit) {
					
						rotateUntilFacingDesiredCD(CardinalDirection.North);
						bot.jump();
				} else if ( bot.controller.getMazeConfiguration().isValidPosition(x, y-1)
					 && isCheaperToJump(x, y, x, (y-1)) 
					 && distance.getDistanceValue(x, y-1) < currentDistanceToExit ) {
					
						rotateUntilFacingDesiredCD(CardinalDirection.South);
						bot.jump();
				} else 
					tryToMoveInstead(x, y); // can afford jump, but moving is still better choice
				
			} else /* Can't afford jump */
				tryToMoveInstead(x, y);
		} // end main while loop

		if ( bot.hasStopped() )
			return false;
		// Bot should be at the exit cell or stopped at this point
		// turn the bot to face the exit, and walk through
		while ( !bot.hasStopped() && !bot.canSeeThroughTheExitIntoEternity(Direction.FORWARD) )
			bot.rotate(Turn.LEFT); // do left turns until facing exit
		if ( !bot.hasStopped() )
			bot.move(1, false); // move through exit. This can cause crashes without the hasStopped check above
	
		return !bot.hasStopped(); // can't return true explicitly because the bot could've stopped trying the last move
	}

	@Override
	public float getEnergyConsumption() {
		return MAX_BATTERY - bot.getBatteryLevel(); // total - remaining = what was used
	}

	@Override
	public int getPathLength() {
		return bot.getOdometerReading();
	}
	
	/**
	 * Rotates the robot until it is facing the direction it has decided to go in.
	 * This is used for both moves and jumps, and for the final move through the exit.
	 * @param cd
	 */
	private void rotateUntilFacingDesiredCD(CardinalDirection cd) {
		while (bot.controller.getCurrentDirection() != cd)
			bot.rotate(Turn.RIGHT);
	}
	/**
	 * Calculates how much closer to the exit jumping to the given destination would
	 * put the robot. If the cost of moving 1 cell at a time is cheaper, it will return false.
	 * If jumping is cheaper, it will return true.
	 * This method does not take into account rotations while moving, or their cost.
	 * @param x the current/starting x coordinate
	 * @param y the current/starting y coordinate
	 * @param ex the ending x coordinate (x coordinate after jumping)
	 * @param ey the ending y coordinate (y coordinate after jumping)
	 * @return
	 */
	private boolean isCheaperToJump(int x, int y, int ex, int ey) {
		int distanceDifference = distance.getDistanceValue(x, y) - distance.getDistanceValue(ex, ey);
		if ( bot.JUMP_COST < (bot.MOVE_COST * distanceDifference) )
			return true;
		return false;
	}
	
	/**
	 * This method is called when the wizard decides it is better to
	 * move 1 step at a time instead of jumping to a location.
	 * The move will be away from the given x, y position toward the exit.
	 * @param x, the current x position of the robot
	 * @param y, the current y position of the robot
	 */
	private void tryToMoveInstead(int x, int y) {
		int[] targetCell = bot.controller.getMazeConfiguration().getNeighborCloserToExit(x, y);
		CardinalDirection directionToTarget = null;
		
		// Determine direction target cell in is
		if      ( x+1 == targetCell[0] )
			directionToTarget = CardinalDirection.East;
		else if ( x-1 == targetCell[0] )
			directionToTarget = CardinalDirection.West;
		else if ( y+1 == targetCell[1] )
			directionToTarget = CardinalDirection.South; // Fixed, north and south were swapped
		else if ( y-1 == targetCell[1] )
			directionToTarget = CardinalDirection.North;
		assertNotNull(directionToTarget); // direction should always be one of the 4 CD's if correct
		
		// rotate until bot is facing the target cell
		rotateUntilFacingDesiredCD(directionToTarget);
		if ( !bot.hasStopped() )
			bot.move(1, false);		
	}
}
