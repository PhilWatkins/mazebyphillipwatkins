package gui;

import generation.Distance;
import gui.Constants.UserInput;
import gui.Robot.Direction;
import gui.Robot.Turn;
/**
 * This driver will try to reach the exit by following the wall on its left.
 * In the event that a sensor fails, the robot turns around (180 degrees) so that its
 * right sensor faces the left wall and can act as its "left" sensor, and
 * its backward sensor will face forward and can act as its "front" sensor.
 * The standard state is when the wizard has operational front and left sensors.
 * If the wizard is not in its standard state, it will walk backwards and follow the same
 * left wall, but will follow it with its right sensor.
 * @author Phillip Watkins
 *
 */
public class WallFollower implements RobotDriver {
	private BasicRobot bot;
	private int w;
	private int h;
	private Distance distance; // not used by this driver
	private float MAX_BATTERY = 3000; // for calculating energy consumption
	
	// variables that change depending on the state
	// all values are initially set to the standard state (working front AND left sensors)
	private boolean[] workingSensors = { true, true, true, true }; // ordered left, front, right, left
	private Direction[] validDirections = { Direction.LEFT, Direction.FORWARD };
	private Direction directionMoving = Direction.FORWARD; // changed to backward if not in standard state
	private boolean inStandardState = true; // functions normally and uses front and left sensors by default
	
	public WallFollower() {
	}
	
	@Override
	public void setRobot(Robot r) {
		bot = (BasicRobot)r;
	}

	@Override
	public void setDimensions(int width, int height) {
		w = width;
		h = height;
	}

	@Override
	public void setDistance(Distance distance) {
		this.distance = distance;
	}

	@Override
	public void triggerUpdateSensorInformation() {
		// adjust driver's sensor knowledge to match bot's
		// JButton will have 3 seconds delay before causing updates
		updateState(); // forces WallFollower to update its state to match the new status of the sensors
	}
	
	@Override
	public boolean drive2Exit() throws Exception {
		// show full map and yellow path to exit by default, except when there is no panel to operate
		if ( bot.controller.panel != null) {
			bot.controller.keyDown(UserInput.ToggleLocalMap, 0);
			bot.controller.keyDown(UserInput.ToggleFullMap, 0);
			bot.controller.keyDown(UserInput.ToggleSolution, 0);
		}
		
		// find a wall to follow if spawned in the center of a room
		dealWithRooms();
		
		while ( !bot.hasStopped() && !bot.isAtExit() ) {
			updateState(); // double check that current state is correct
			try { // try to sense for walls
				if( bot.distanceToObstacle(validDirections[0]) == 0 ) { // has wall to its "left"
					if( bot.distanceToObstacle(validDirections[1]) != 0 ) // has no wall "forward", can move
						if ( inStandardState ) bot.move(1, false); else bot.moveBackward(1, false);
					else
						bot.rotate(Turn.RIGHT); // can't move yet--turn right and try again in next loop iteration
				}

				// else there is no wall on its "left"
				else {
					bot.rotate(Turn.LEFT); // rotate and move around corner
					if ( inStandardState ) bot.move(1, false); else bot.moveBackward(1, false);
				}
			
			} catch (Exception e) { // realize a needed sensor has failed
				updateState(); // adjust to sensor failure and keep going in the next iteration
			}
		} // end while block
		
		if ( bot.hasStopped() )
			return false;
		// do left turns until facing exit
		while ( !bot.hasStopped() && !bot.canSeeThroughTheExitIntoEternity(directionMoving) )
			bot.rotate(Turn.LEFT);
		
		// proceed through the exit (moving backward though exit is okay)
		if (inStandardState) bot.move(1, false); else bot.moveBackward(1, false);
		
		return !bot.hasStopped();
	}

	@Override
	public float getEnergyConsumption() {
		return MAX_BATTERY - bot.getBatteryLevel(); // total - remaining = what was used
	}

	@Override
	public int getPathLength() {
		return bot.getOdometerReading();
	}
	
	/**
	 * Adjust variables that represent the state. Also will turn the robot around
	 * when the state changes.
	 */
	private void updateState() {
		if ( bot.hasOperationalSensor(Direction.FORWARD) && bot.hasOperationalSensor(Direction.LEFT) ) {
			if ( !inStandardState ) {
				bot.rotate(Turn.AROUND); // turn around, state has reversed
				System.out.println("Going back to using front and left sensors");
			}
			inStandardState = true;
			workingSensors[0] = true;
			workingSensors[1] = true;
			directionMoving = Direction.FORWARD;
			validDirections[0] = Direction.LEFT;
			validDirections[1] = Direction.FORWARD;
		
		} else  if ( bot.hasOperationalSensor(Direction.BACKWARD) || bot.hasOperationalSensor(Direction.RIGHT) ){
			if( inStandardState ) {
				bot.rotate(Turn.AROUND); // turn around, state have reversed
				System.out.println("Adjusting to failed front/left sensor");
			}
			
			inStandardState = false;
			workingSensors[0] = true;
			workingSensors[1] = true;
			directionMoving = Direction.BACKWARD;
			validDirections[0] = Direction.RIGHT;
			validDirections[1] = Direction.BACKWARD;
		} 
	}
	
	/**
	 * Handles situations where robot spawns inside a room by
	 * walking to nearest wall, and then turning until a followable wall is
	 * on its left.
	 */
	public void dealWithRooms() {
		// If inside room walk to the nearest wall
		while ( bot.isInsideRoom() && bot.distanceToObstacle(directionMoving) != 0 )
			bot.move(1, false);
		
		// turn until there is a wall to follow
		while ( bot.distanceToObstacle(validDirections[0]) != 0 )
			bot.rotate(Turn.LEFT);
	}
}
