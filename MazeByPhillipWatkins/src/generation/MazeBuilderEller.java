package generation;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MazeBuilderEller extends MazeBuilder implements Runnable{

	public int[][] currentLayout; // 2D array representing each cell.
								  // cells containing the same number
								  // are members of the same set
	
	public MazeBuilderEller(boolean deterministic) {
		super(deterministic); // calls superclass constructor to create a deterministic maze
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze. (deterministic)");
	}
	
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze. (random)");
	}
	
	/**
	 * Initializes a 2D array of dimensions width * height such that
	 * each cell contains a unique number;
	 * Cells containing the same number are part of the same set.
	 */
	public void initializeSets() {
		currentLayout = new int[width][height]; // Floorplan says [width][height] not [h][w]??????????

		int k = 1;
		for (int i=0; i<width; i++) {
			for (int j=0; j<height; j++) {
				currentLayout[i][j] = k;
				k++;
			}
		}
	}
	/**
	 * This method gets called from MazeBuilder to create pathways through the maze
	 * using Eller's algorithm.
	 * <p>
	 * It loops through each row and deletes east walls randomly (50/50 chance)
	 * when the cell and the cell to its right are in different sets.
	 * Since each set must also contain a path downward (must be missing a south wall)
	 * an additional loop goes through each row and adds sets to a "visited" set
	 * to mark them as such. Each new set gets an ArrayList containing
	 * the indices of all members in the current row. From these indices, 
	 * random south walls are chosen to be removed so that each set has at least 
	 * 1 missing south wall (and thus a path downward to prevent isolations).
	 * For the final row, a loop goes through each cell and tears down walls
	 * whenever the cells are in different sets. Walls will be torn down liberally
	 * since this is the last opportunity to ensure the maze is connected.
	 */
	@Override
	public void generatePathways() { // called in MazeBuilder class
		initializeSets(); // puts each cell in its own set			
		Wallboard curEast; 
		Wallboard curSouth;
		int rand; // will be set to a random value as needed
		boolean lastRow = false; // Special case: Don't wait until bottom walls are deleted if last row
		
		for (int y=0; y<height; y++) { // 1 row at a time
			
			if (y == height-1)
				lastRow = true;
			
			if (!lastRow) {
				for (int x=0; x<width-1; x++) {
					curEast = new Wallboard(x, y, CardinalDirection.East);
					
					if (currentLayout[x][y] != currentLayout[x+1][y]) {
						rand = random.nextIntWithinInterval(0, 100); // random int from 0-99
						if (rand > 50) {
							
							if (floorplan.canTearDown(curEast)) {
								floorplan.deleteWallboard(curEast);
								currentLayout[x+1][y] = currentLayout[x][y]; // make sets the same
							}
						}
					} 
				} // end x loop	

		// compile list of indices of cells in this row with same value (indicating they're in the same set)
				Set<Integer> visited = new HashSet<Integer>();
				for (int x=0; x<width; x++) {
					int target = currentLayout[x][y]; // set we're looking for members of
					
					if (visited.contains(target))
						continue; // already dealt with that set, skip it
					
					visited.add(target);
					ArrayList<Integer> indices = new ArrayList<>(); // indices of set members in current row only
					
					for (int k=x; k<width; k++)
						if (currentLayout[k][y] == target) // if value is in the same set as target
							indices.add(k);// add the index of that value
	
					int len = indices.size(); // len of array containing indices of set members. always at least 1

					if (len == 1) { // only member of its set, MUST tear down south wall
						curSouth = new Wallboard(indices.get(0), y, CardinalDirection.South);
						floorplan.deleteWallboard(curSouth); // don't care if wall is part of room
						currentLayout[indices.get(0)][y+1] = currentLayout[indices.get(0)][y];
					}
					else {						
						int rando = 1;
						while (rando > 0) {
						// remove south walls at random index among the choices of indices
							int temp = indices.get(rando % len);
							curSouth = new Wallboard(temp, y, CardinalDirection.South);
							rando--;
							floorplan.deleteWallboard(curSouth);
							currentLayout[temp][y+1] = currentLayout[temp][y];
						}
					}
				}
			}	
			
			if (lastRow) { // last row, tear down walls between cells that aren't connected yet
				for (int i=0; i<width-1; i++) {
					if (currentLayout[i][y] != currentLayout[i+1][y]) {
						curEast = new Wallboard(i, y, CardinalDirection.East);
						
						if (floorplan.canTearDown(curEast)) {
							floorplan.deleteWallboard(curEast);
							currentLayout[i+1][y] = currentLayout[i][y];
						}
					}
				}
			}
			
		} // end y loop
	} // end function
} // end class
