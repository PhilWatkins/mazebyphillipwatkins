package gui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import gui.Robot.Direction;
/**
 * This class is for testing the WallFollower driver. Test make use of 
 * the maze from input.xml for consistency.
 * @author Phillip Watkins
 *
 */
public class WallFollowerTest {

	Controller controller;
	Robot bot;
	RobotDriver driver;
	
	/**
	 * Sets up controller, robot, and driver to operate on hard-coded
	 * maze from file input.xml.
	 * This method should be used to set up for tests with "FromFile" in their name.
	 */
	public void setUpFromFile() {
		bot = new BasicRobot();
		driver = new WallFollower();
		controller = new Controller();
		bot.setMaze(controller);
		driver.setRobot(bot);
		controller.turnOffGraphics();
		// artificially transition through states
		controller.switchFromTitleToGenerating("test/data/input.xml");
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		controller.setRobotAndDriver(bot, driver);
	}

	/**
	 * This is a basic test to check if the robot can reach the exit. It allows
	 * all possible sensors to be used to reach the exit. The exit should be 
	 * reached every time because the maze and starting location are hard-coded.
	 * @throws Exception
	 */
	@Test
	public void testReachesExitFromFile() throws Exception {
		setUpFromFile();
		System.out.println("Moving through another maze... It may take the WallFollower 30-45 seconds to reach the exit.");
		assertTrue(driver.drive2Exit());
	}

	/**
	 * Adjusts the battery level to 10 so that the robot can't possibly reach the
	 * exit without stopping/crashing. The driver should not reach the exit and the robot
	 * should be stopped.
	 * @throws Exception
	 */
	@Test
	public void testFailureWhenExpected() throws Exception {
		setUpFromFile();
		bot.setBatteryLevel(10); 
		assertFalse(driver.drive2Exit());
		assertTrue(bot.hasStopped());
		System.out.println("The robot was expected to crash.");
	}
	/**
	 * Deactivates both the front and left sensors and checks if the 
	 * wall follower can adjust and still reach the exit.
	 */
	@Test
	public void testWithNoFrontOrLeftSensor() throws Exception {
		setUpFromFile();
		// Leave only the back and right sensors active
		bot.triggerSensorFailure(Direction.FORWARD);
		bot.triggerSensorFailure(Direction.LEFT);
		driver.triggerUpdateSensorInformation(); // have to make WallFollower aware of the sensor failures
		// robot should still make it to the exit
		System.out.println("Driving through input.xml maze with no front or left sensor...");
		System.out.println("It may take the WallFollower 30-45 seconds to reach the exit.");
		try {
			driver.drive2Exit();
		} catch (Exception e) {
			
		}
	}
}
