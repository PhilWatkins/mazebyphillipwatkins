package gui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * This class tests the Wizard driver for functionality and correctness.
 * Most tests are done on the maze from file input.xml for consistency. 
 * 
 * @author Phillip Watkins
 *
 */
public class WizardTest {

	Controller controller;
	Robot bot;
	RobotDriver driver;
	/**
	 * Sets up a maze from file input.xml for testing. 
	 * This setUp does not support graphics.
	 */
	public void setUpFromFile() {
		bot = new BasicRobot();
		driver = new Wizard();
		controller = new Controller();
		bot.setMaze(controller);
		driver.setRobot(bot);
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating("test/data/input.xml");
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
		controller.setRobotAndDriver(bot, driver);
	}
	/**
	 * Tests if the wizard can reach the exit on a hard coded maze 
	 * from file input.xml. This test does not check if the path was the shortest one 
	 * possible, so the exit may be reached without the wizard taking the optimal.
	 * @throws Exception
	 */
	@Test
	public void testBasicCanFindExit() throws Exception {
		setUpFromFile();
		assertTrue(driver.drive2Exit());
	}
	
	/**
	 * Checks if the pathlength the wizard took is the shortest.
	 * Input.xml has a min path length to exit of 8 when jumps are allowed,
	 * so the wizard should reach the exit in 8 moves/jumps.
	 * This test will fail if jumps are not supported yet because
	 * the minimum path to the exit involves jumping the wall
	 * to the west of the bot's starting position.
	 * @throws Exception
	 */
	@Test
	public void testIfShortestPath() throws Exception {
		setUpFromFile();
		driver.drive2Exit();
		// Should jump west wall, move west until at border,
		// move up until at exit cell, then move through exit
		assertEquals(bot.getOdometerReading(), 8); 
	}
}