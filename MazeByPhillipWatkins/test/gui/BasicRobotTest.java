package gui;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.Test;

import generation.CardinalDirection;
import gui.Robot.Direction;
import gui.Robot.Turn;
/**
 * Test cases for BasicRobot (P3).
 * These tests are all done on the maze from input.xml.
 * Graphics are turned off also.
 * @author Phillip Watkins
 *
 */
public class BasicRobotTest{
	Controller controller;
	BasicRobot bot;
	
	/**
	 * Sets up robot and controller to explore maze from file input.xml
	 * Graphics are turned off for each test.
	 */
	public final void setUp() {
		bot = new BasicRobot();
		controller = new Controller();
		bot.setMaze(controller);
		controller.turnOffGraphics();
		controller.switchFromTitleToGenerating("test/data/input.xml");
		controller.switchFromGeneratingToPlaying(controller.getMazeConfiguration());
	}
	
	/**
	 * Tests if an Exception is thrown when the robot tries to jump the south
	 * border wall.
	 * @throws Exception
	 */
	@Test
	public final void testExceptionWhenJumpingNorthBorderWall() throws Exception{
		setUp();
		bot.rotate(Turn.RIGHT); // robot now faces border wall at bottom of screen
		assertThrows(Exception.class, () -> {
			bot.jump(); // should throw an exception because its a border wall
		});
	}
	
	/**
	 * Tests if the robot can turn around and jump the wall to the west
	 * of the hard-coded starting position. Makes used of rotate(), getCurrentPosition().
	 * This should not throw an exception because jumping the west wall is valid.
	 * @throws Exception
	 */
	@Test
	public final void testJumpSuccessToWestOfStartingPosition() throws Exception {
		setUp();
		int[] start = bot.getCurrentPosition();
		bot.rotate(Turn.AROUND);
		assertTrue(bot.getCurrentDirection() == CardinalDirection.West); // should be facing top of screen after rotating
		bot.jump();
		int[] stopped = bot.getCurrentPosition();
		assertTrue( (start[0]-1) == stopped[0] ); // stop position should have x value one less than start
		assertTrue( bot.getBatteryLevel() == 3000 - (2*bot.ROTATE_COST) - bot.JUMP_COST );
	}
	
	/**
	 * Tests if bot can jump north from hard-coded starting position. The jump should be successful.
	 * @throws Exception
	 */
	@Test
	public final void testJumpSuccessToNorthOfStartingPosition() throws Exception {
		setUp();
		int[] start = bot.getCurrentPosition();
		bot.rotate(Turn.LEFT); // turns robot to face North (where the wall is not part of border)
		bot.jump();
		int[] stopped = bot.getCurrentPosition();
		assertTrue(start[1] == 0);
		assertTrue(stopped[1] == 1);
	}
	
	/**
	 * Explicitly tests that the maze was initialized from file input.xml.
	 * This may fail if the controller didn't switch from state to state properly.
	 */
	@Test
	public final void testMazeInitializedFromFile() {
		setUp();
		assertNotNull(controller.getMazeConfiguration());
	}
	
	/**
	 * Have bot jump back and forth until battery is depleted.
	 * It should be stopped afterward if behavior is correct.
	 * @throws Exception
	 */
	@Test
	public final void testStopWhenBatteryDepleted() throws Exception {
		setUp();
		bot.setBatteryLevel(512); // takes too long with 3000 battery. 500 is plenty for testing.
		while( bot.hasStopped() == false ) {
			bot.rotate(Turn.AROUND);
			if ( !bot.hasStopped() )
				bot.jump();
		}
		assertTrue( bot.hasStopped() );
		
		// calls below should do nothing, but shouldn't crash the program
		bot.move(1, true);
		bot.jump();
		bot.rotate(Turn.AROUND);
	}
	
	/**
	 * Checks if battery level is correctly set, and if getBatteryLevel() can access it.
	 */
	@Test
	public final void testBatteryLevelInitialized() {
		setUp();
		assertEquals(bot.getBatteryLevel(), 3000);
	}
	
	/**
	 * Tests that the robot's battery decreases after a jump.
	 * @throws Exception
	 */
	@Test
	public final void testJumpBatteryConsumption() throws Exception {
		setUp();
		bot.rotate(Turn.AROUND);
		bot.jump();
		assertEquals( bot.getBatteryLevel(), 3000 - bot.JUMP_COST - (2*bot.ROTATE_COST) );
	}
	
	/**
	 * Checks if battery gets depleted correctly from a single move operation.
	 */
	@Test
	public final void testMoveBatteryConsumption() {
		setUp();
		bot.move(1, true);
		assertEquals(bot.getBatteryLevel(), 3000 -  bot.MOVE_COST );
		assertEquals(bot.getBatteryLevel(), 2995); // should be 5 less than 3000
	}
	
	/**
	 * Makes robot perform a simple move in the direction it's facing at the start.
	 * A more rigid test is done with testMoveInComplicatedPattern() located below.
	 */
	@Test
	public final void testSimpleMove() {
		setUp();
		int[] start = controller.getCurrentPosition();
		bot.move(5, true);
		assertTrue(controller.getCurrentPosition() != start);
	}
	
	/**
	 * Moves the robot through a hard-coded path to the top right corner (on screen)
	 * of input.xml maze. This exercises sensing and moving in several directions.
	 * There also is a test to see if jumping the East or North border walls
	 * throws an exception since only this method puts the robot near enough to jump them.
	 * @throws Exception
	 */
	@Test 
	public final void testMoveInComplicatedPattern() throws Exception {
		setUp();
		bot.move(21, true);
		bot.rotate(Turn.LEFT);

		// hard coded navigation through input.xml
		bot.move(2, true);
		bot.rotate(Turn.RIGHT);
		bot.move(2, true);
		bot.rotate(Turn.LEFT);
		bot.move(5, true);
		bot.rotate(Turn.LEFT);
		bot.move(1, true);
		bot.rotate(Turn.RIGHT);
		bot.move(1, true);
		bot.rotate(Turn.RIGHT);
		bot.move(3, true);
		bot.rotate(Turn.LEFT);
		bot.move(6,  true);
		
		// Jump tests since bot is positioned conveniently in the top right corner of screen
		assertThrows(Exception.class, () -> {
			bot.jump(); // Jumping North wall should throw an exception
		});
		bot.rotate(Turn.RIGHT);
		assertThrows(Exception.class, () -> {
			bot.jump(); // Jumping East wall should throw an exception
		});
		
		bot.rotate(Turn.RIGHT);
		bot.move(1, false);
	}
	
	/**
	 * Starts with a robot facing East. Rotates left 4 times and checks
	 * if the robot is facing in the correct direction after each rotation.
	 * 4 rotations should put the robot back in its starting position, and 
	 * the battery level should decrease by 3 each time.
	 */
	@Test
	public final void testRotatesLeft() {
		setUp();
		
		// Starts facing east always
		bot.rotate(Turn.LEFT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.South);
		
		bot.rotate(Turn.LEFT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.West);
		
		bot.rotate(Turn.LEFT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.North);
		
		bot.rotate(Turn.LEFT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.East);
	}
	
	/**
	 * Starts with a robot facing East. Rotates RIGHT four times, checking
	 * is the battery level decreases and the robot faces in the correct direction
	 * after each. 4 rotations should put the robot back to its starting position.
	 */
	@Test
	public final void testRotatesRight() {
		setUp();
		
		bot.rotate(Turn.RIGHT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.North);
		
		bot.rotate(Turn.RIGHT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.West);
		
		bot.rotate(Turn.RIGHT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.South);
		
		bot.rotate(Turn.RIGHT);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.East);
	}
	
	/**
	 * Starts with a robot facing east, checks if rotating AROUND properly
	 * decreases the battery level and changes the direction the robot is facing in.
	 */
	@Test
	public final void testRotatesAround() {
		setUp();
		
		bot.rotate(Turn.AROUND);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.West);
		assertEquals(bot.getBatteryLevel(), 3000 - (2*bot.ROTATE_COST)); // cost is doubled for AROUND rotation
		
		bot.rotate(Turn.AROUND);
		assertEquals(bot.getCurrentDirection(), CardinalDirection.East);
		assertEquals(bot.getBatteryLevel(), 3000 - (4*bot.ROTATE_COST));
		
	}
	/**
	 * Uses each sensor method at least once to check if sensors are active,
	 * deactivated successfully, and then reactivated.
	 */
	@Test
	public final void testSensors() {
		setUp();
		// start by checking if all sensors are working initially
		assertTrue(bot.hasOperationalSensor(Direction.FORWARD));
		assertTrue(bot.hasOperationalSensor(Direction.BACKWARD));
		assertTrue(bot.hasOperationalSensor(Direction.LEFT));
		assertTrue(bot.hasOperationalSensor(Direction.RIGHT));
		// deactivate sensors in each direction
		bot.triggerSensorFailure(Direction.FORWARD);
		bot.triggerSensorFailure(Direction.BACKWARD);
		bot.triggerSensorFailure(Direction.LEFT);
		bot.triggerSensorFailure(Direction.RIGHT);
		assertFalse(bot.hasOperationalSensor(Direction.FORWARD));
		assertFalse(bot.hasOperationalSensor(Direction.BACKWARD));
		assertFalse(bot.hasOperationalSensor(Direction.LEFT));
		assertFalse(bot.hasOperationalSensor(Direction.RIGHT));
		// repair sensor in each direction
		assertFalse(bot.repairFailedSensor(Direction.FORWARD));
		assertFalse(bot.repairFailedSensor(Direction.BACKWARD));
		assertFalse(bot.repairFailedSensor(Direction.LEFT));
		assertFalse(bot.repairFailedSensor(Direction.RIGHT));
		// check if each sensor is operational again
		assertTrue(bot.hasOperationalSensor(Direction.FORWARD));
		assertTrue(bot.hasOperationalSensor(Direction.BACKWARD));
		assertTrue(bot.hasOperationalSensor(Direction.LEFT));
		assertTrue(bot.hasOperationalSensor(Direction.RIGHT));
		
		// check if repairing an operational sensor returns true
		assertTrue(bot.repairFailedSensor(Direction.FORWARD));
		assertTrue(bot.repairFailedSensor(Direction.BACKWARD));
		assertTrue(bot.repairFailedSensor(Direction.LEFT));
		assertTrue(bot.repairFailedSensor(Direction.RIGHT));
		
		assertTrue(bot.hasRoomSensor());
		assertFalse(bot.isInsideRoom());
		
		bot.decideWallDirection(Direction.BACKWARD);
		bot.decideWallDirection(Direction.FORWARD);
		bot.decideWallDirection(Direction.LEFT);
		bot.decideWallDirection(Direction.RIGHT);
		
	}
	/**
	 * Positions the robot near the exit to check if it identifies it.
	 * Also checks if the robot doesn't identify the exit when it isn't facing it yet.
	 * This involved moving the robot to the bottom left corner of the screen, 
	 * so I took advantage of the bot's position and checked if trying to jump the west border
	 * wall throws an exception.
	 * @throws Exception
	 */
	@Test
	public final void testExitIdentified() throws Exception {
		setUp();
		bot.rotate(Turn.AROUND); // face toward (0,0) at bottom left
		bot.jump(); // jump west wall
		bot.move(3, true); // move to (0,0)
		bot.rotate(Turn.RIGHT); // turn an face exit
		assertFalse(bot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)); // exit is visible, but to bot's left
		
		
		// Conveniently positioned in bottom left corner. Might as well check if
		// an exception is thrown when jumping the West border
		bot.rotate(Turn.LEFT);
		assertThrows(Exception.class, () -> {
			bot.jump(); // should throw an exception because west wall is part of border.
		});
		bot.rotate(Turn.RIGHT);
		
		// Move to exit and try to identify it
		bot.move(3, true);
		assertTrue(bot.isAtExit());
		bot.rotate(Turn.LEFT);
		assertTrue(bot.canSeeThroughTheExitIntoEternity(Direction.FORWARD)); // exit is visible
	}
	
	/**
	 * Tests most of the one-liners like getters/setters that don't need
	 * their own tests.
	 */
	@Test
	public final void testSimpleMethods(){
		setUp();
		// odometer tests
		assertEquals(bot.getOdometerReading(), 0);
		bot.move(1, false);
		assertEquals(bot.getOdometerReading(), 1);
		bot.resetOdometer();
		assertEquals(bot.getOdometerReading(), 0);
		
		// getEnergy... tests
		assertEquals(bot.getEnergyForFullRotation(), 4*bot.ROTATE_COST);
		assertEquals(bot.getEnergyForStepForward(), bot.MOVE_COST);
		
		assertFalse(bot.isAtExit());
	}
	
	/**
	 * Deactivates forward sensor and checks if an UnsupportedOperationException is thrown
	 * when trying to determine forward without a sensor.
	 * @throws UnsupportedOperationException
	 */
	@Test
	public final void testDistanceSensingWithBrokenSensor() throws UnsupportedOperationException {
		setUp();
		bot.triggerSensorFailure(Direction.FORWARD);
		bot.triggerSensorFailure(Direction.BACKWARD);
		bot.triggerSensorFailure(Direction.LEFT);
		bot.triggerSensorFailure(Direction.RIGHT);
		assertThrows(UnsupportedOperationException.class, () -> {
			// all should throw exceptions
			bot.distanceToObstacle(Direction.FORWARD);
			bot.distanceToObstacle(Direction.BACKWARD);
			bot.distanceToObstacle(Direction.LEFT);
			bot.distanceToObstacle(Direction.RIGHT);
			bot.canSeeThroughTheExitIntoEternity(Direction.BACKWARD);
		});
		
	}
}
