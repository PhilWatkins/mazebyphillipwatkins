package generation;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class MazeFactoryEllerTest {

	private Maze finalMaze;
	private MazeFactory myFactory;
	private StubOrder order1;
	
	/**
	 * Sets up, builds, and delivers an imperfect maze generated using Eller's algorithm.
	 * Remember that an imperfect maze can have cycles, and rooms need to be partially
	 * torn down if required by Eller's.
	 */
	public final void setUp() {
		myFactory = new MazeFactory(true);
		order1 = new StubOrder();
		order1.setBuildType(Order.Builder.Eller); // always will be Eller
		myFactory.order(order1);
		myFactory.waitTillDelivered();
		finalMaze = order1.getMaze();
	}
	
	/**
	 * Sets up a perfect maze for testing. 
	 */
	public final void setUpPerfect() {
		myFactory = new MazeFactory(true);
		order1 = new StubOrder();
		order1.setBuildType(Order.Builder.Eller);
		order1.setToPerfect(); // not included in other setup; setup creates an imperfect maze by default
		myFactory.order(order1);
		myFactory.waitTillDelivered();
		finalMaze = order1.getMaze();
	}
	
	/**
	 * Tests if each cell has a path to the exit for a perfect maze generated with Eller's alg.
	 * If there is an enclosed area, this test will fail.
	 */
	@Test
	public final void testNoIsolationsWithoutRooms() {
		setUpPerfect();
		
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();
		int cap = h*w;
		for (int i=0; i<w; i++) {
			for (int j=0; j<h; j++) {
				assertTrue(cap > finalMaze.getDistanceToExit(i, j));
			}
		}
		tearDown();
	}
	
	/**
	 * Tests if a maze with rooms & created with Eller's algorithm
	 * can be properly initialized. This requires the skill level
	 * to be at least 1 to work properly, as a maze
	 * created on skill level zero is too small to contain rooms
	 * within its boundaries.
	 */
	@Test
	public final void testInitializeWithRooms() {
		setUp(); // isPerfect returns false by default in StubOrder
		assertNotNull(finalMaze);
		assertNotNull(finalMaze.getClass());
		assertNotNull(finalMaze.getFloorplan());
		assertNotNull(finalMaze.getHeight());
		assertNotNull(finalMaze.getWidth());
		assertNotNull(finalMaze.getMazedists());
		assertNotNull(finalMaze.getRootnode());
		assertNotNull(finalMaze.getStartingPosition());
		tearDown();
		
	}
	/**
	 * Checks if the generation of rooms causes parts of the maze to be isolated.
	 * Rooms should allow cycles. Also note that rooms will not be generated
	 * when the skill level is zero. The skill level should be > 0 for this
	 * test to be useful.
	 */
	@Test
	public final void testConnectivityWithRoomsPresent() {
		setUp(); // rooms are enabled by default
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();
		int cap = w*h; // distance to exit should be shorter than traversing the entire maze
		for (int x=0; x<w; x++) {
			for (int y=0; y<h; y++) {
				assertTrue(cap > finalMaze.getDistanceToExit(x, y));
			}
		}
	}
	
	public final void tearDown() {
		order1 = null;
		finalMaze = null;
		myFactory = null;
	}
	
	/**
	 * Tests if the majority of the walls remain in tact when a perfect maze is generated.
	 * There isn't a strict threshold for how many walls are needed to make the maze interesting, but
	 * having more walls stay up than get torn down seems like a reasonable measure.
	 */
	@Test
	public final void testSufficientNumberOfWallsRemainPerfect() {
		setUpPerfect();
		
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();
		Floorplan myPlan = finalMaze.getFloorplan();
		int wallCount = 0;
		int noWallCount = 0;
		for (int x=0; x<w; x++) {
			for (int y=0; y<h-1; y++) {
			// logic below increments wallCount if there's a wall, increments noWall if no wall exists there
				if (myPlan.hasWall(x, y, CardinalDirection.East))
					wallCount++;
				else 
					noWallCount++;
				if (myPlan.hasWall(x, y, CardinalDirection.West))
					wallCount++;
				else
					noWallCount++;
				if (myPlan.hasWall(x, y, CardinalDirection.North))
					wallCount++;
				else
					noWallCount++;
				if (myPlan.hasWall(x, y, CardinalDirection.South)) 
					wallCount++;
				else
					noWallCount++;
			}
		}
		System.out.println("\nNumber of walls left up: " + wallCount);
		System.out.println("Number of walls torn down: " + noWallCount);
		assertTrue(wallCount > noWallCount); // no rooms, most walls should be left up to make the maze interesting
		tearDown();
	}
	
}
