package generation;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

/**
 * This class tests the functionality of MazeFactory. MazeFactory tests should
 * pass regardless of which build algorithm is used. Setting the buildType to DFS or Prim
 * should not change the outcome of tests. Kruskal's algorithm, however, is not implemented
 * and not intended to be tested.
 * @author phil
 *
 */
public class MazeFactoryTest {
		
	private MazeFactory myFac;
	private StubOrder myOrder;
	private Maze finalMaze; // This is the delivered maze for testing
	
	/**
	 * Sets up a Perfect Maze using the given algorithm (Change Eller to Prim or DFS as needed)
	 */
	private void setUpPerfect() {
		myFac = new MazeFactory(true);
		myOrder = new StubOrder();
		myOrder.setToPerfect(); // sets maze to perfect unlike other setUp() method
		myOrder.setToPerfect();
		myOrder.setBuildType(Order.Builder.Eller); // can be changed to (Prim, Eller, DFS)
		myFac.order(myOrder);
		myFac.waitTillDelivered();
		finalMaze = myOrder.getMaze();
	}
	
	/**
	 * Sets up and builds a maze with rooms enabled.
	 */
	private void setUp() {
		myFac = new MazeFactory(true);
		// maze is not set to perfect here, unlike the other setUp() method
		myOrder = new StubOrder();
		myOrder.setBuildType(Order.Builder.Eller); // can be changed to (Prim, Eller, DFS)
		myFac.order(myOrder);
		myFac.waitTillDelivered();
		finalMaze = myOrder.getMaze();
	}
	
	/**
	 * Tears down the maze. This gets called at the end of every test.
	 */
	private void tearDown() {
		myFac = null;
		myOrder = null;
		finalMaze = null;
	}

	/**
	 * Tests if maze has all the necessary components initialized.
	 * If any value is null, the maze isn't playable.
	 */
	@Test
	public final void testInitialized() {
		setUp();
		assertNotNull(finalMaze.getFloorplan());
		assertNotNull(finalMaze.getHeight());
		assertNotNull(finalMaze.getMazedists());
		assertNotNull(finalMaze.getRootnode());
		assertNotNull(finalMaze.getStartingPosition());
		assertNotNull(finalMaze.getWidth());
		tearDown();
	}
	
	/**
	 * Checks if there is a path to the exit from each cell in the maze. 
	 * This should be true even if there are rooms-- rooms should not cause
	 * isolations, even when using Eller's algorithm.
	 */
	@Test
	public final void testPathToExitFromAnywherePerfect() {
		setUpPerfect();
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();
		int cap = w*h; // shortest path shouldn't involve visiting each cell. cap differs with maze dimensions
		for (int i=0; i<w; i++) {
			for (int j=0; j<h; j++) {
				assertTrue(cap > finalMaze.getDistanceToExit(i, j));
			}
		}
		tearDown();	
	}
	
	/**
	 * Checks if each cell has a path to the exit even when rooms are enabled.
	 * Rooms should not cause any parts of the maze to be isolated.
	 */
	@Test
	public final void testStillConnectedWithRooms() {
		setUp();
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();
		int cap = w*h;
		for (int i=0; i<w; i++) {
			for (int j=0; j<h; j++) {
				assertTrue(cap > finalMaze.getDistanceToExit(i, j));
			}
		}
		tearDown();	
		
	}
	
	/**
	 * Loops through all cells and checks if they have an exit.
	 * The number of exits should always be exactly 1.
	 * If exit count is 0, an exit could not successfully be made.
	 * If exit count is > 1, too many exits were made.
	 */
	@Test
	public final void testOnlyOneExit() {
		setUp();
		Floorplan myPlan = finalMaze.getFloorplan();
		int exitCount = 0;
		int w = finalMaze.getWidth();
		int h = finalMaze.getHeight();

		for (int i = 0; i < w; i++) {
			for (int j=0; j < h; j++) {
				if (myPlan.isExitPosition(i, j)) {
					exitCount++; // should only increment once
				}
			}
		}
		assertEquals(1, exitCount);
		tearDown();
	}


}
